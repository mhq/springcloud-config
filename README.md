# springcloudConfig

#### 介绍

> 提供给 spring cloud config 配置文件的仓库 . 集中管理分布式项目的配置文件 
> 
> server-api => config-server => springcloud-config (GIT仓库地址) 

#### 文件目录

#### miduo-mall

> 米多商城项目 - 配置文件目录
 
1.  order-service
    > 订单服务模块 - 配置文件目录
2.  member-service
    > 会员服务模块 - 配置文件目录


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx